package com.example.mini_jeu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import java.lang.Math;
import java.util.Random;

import androidx.appcompat.app.AppCompatActivity;

public class JeuActivity extends AppCompatActivity {

    private Button button_rules,button_recommencer;

    private ImageButton pierre,feuille,ciseau,lezard,spock;


    private TextView scorejoueur,scoreordinateur,textjeu,symbolechoisi,info;


    int value_scorejoueur=0;
    int value_scoreordinateur=0;
    int nombre_manches=0;








    public void buttonsenabled(boolean state){
        pierre.setClickable(state);
        pierre.setEnabled(state);
        feuille.setClickable(state);
        feuille.setEnabled(state);
        ciseau.setClickable(state);
        ciseau.setEnabled(state);
        lezard.setClickable(state);
        lezard.setEnabled(state);
        spock.setClickable(state);
        spock.setEnabled(state);
        if(state==false){
            pierre.setVisibility(View.GONE);
            feuille.setVisibility(View.GONE);
            ciseau.setVisibility(View.GONE);
            lezard.setVisibility(View.GONE);
            spock.setVisibility(View.GONE);
        }else{
            pierre.setVisibility(View.VISIBLE);
            feuille.setVisibility(View.VISIBLE);
            ciseau.setVisibility(View.VISIBLE);
            lezard.setVisibility(View.VISIBLE);
            spock.setVisibility(View.VISIBLE);
        }
    }

    public void checkendofgame(){
        if(nombre_manches==5){
            info.setText(" ");
            buttonsenabled(false);
            if(value_scorejoueur>value_scoreordinateur){
                textjeu.setText("Vous avez gagné la partie");
            }else{
                textjeu.setText("Vous avez perdu la partie");
            }
            button_recommencer.setVisibility(View.VISIBLE);
        }
    }

    public void jouermanche(String symbole,int difficulty){
        int winrate=(18*difficulty);
        int proba;
        nombre_manches+=1;
        symbolechoisi.setText(""+symbole);
        proba = new Random().nextInt((100 - 0) + 1);
        if(proba>winrate){
            textjeu.setText("vous avez gagné cette manche");
            value_scorejoueur +=1;
            scorejoueur.setText(""+value_scorejoueur);
        }else{
            textjeu.setText("vous avez perdu cette manche");
            value_scoreordinateur += 1;
            scoreordinateur.setText(""+value_scoreordinateur);
        }

    }




    //int difficulty = 1;  //prendre depuis l'activité difficulty.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent recupIntent=getIntent();
        final int difficulty;
        difficulty = recupIntent.getIntExtra("niveau_Difficulte", 0);
        //difficulty=1;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jeu);


        button_rules = findViewById(R.id.button1);
        button_recommencer= findViewById(R.id.button5);
        pierre = findViewById(R.id.imageButton3);
        feuille = findViewById(R.id.imageButton4);
        ciseau = findViewById(R.id.imageButton5);
        lezard = findViewById(R.id.imageButton6);
        spock = findViewById(R.id.imageButton7);


        info=findViewById(R.id.textView9);
        textjeu=findViewById(R.id.textView12);
        symbolechoisi=findViewById(R.id.textView13);
        scorejoueur=findViewById(R.id.textView17);
        scoreordinateur=findViewById(R.id.textView18);






        button_recommencer.setVisibility(View.GONE);
        symbolechoisi.setText("");
        button_rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent conIntent = new Intent(view.getContext(), HelpActivity.class);
                startActivityForResult(conIntent, 0);
            }
        });

        button_recommencer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent conIntent = new Intent(view.getContext(), DifficultyActivity.class);
                startActivityForResult(conIntent, 0);

            }
        });

        pierre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                jouermanche("pierre",difficulty);
                checkendofgame();

            }
        });

        feuille.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                jouermanche("feuille",difficulty);
                checkendofgame();

            }
        });

        ciseau.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                jouermanche("ciseaux",difficulty);
                checkendofgame();

            }
        });

        lezard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                jouermanche("lezard",difficulty);
                checkendofgame();

            }
        });

        spock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                jouermanche("spock",difficulty);
                checkendofgame();

            }
        });






    }
}
