package com.example.mini_jeu;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class ConnexionActivity extends AppCompatActivity {
    private EditText text_mail,text_password;
    private FirebaseAuth fAuth;

    private TextView tv_login,tv_password,texterror;



    private Button button_validerConnexion;



    private void gotomenu(){
        Intent intentMenu = new Intent(ConnexionActivity.this, DifficultyActivity.class);
        startActivity(intentMenu);
    }

    private boolean validation(){
        texterror.setText("");
        boolean valide=true;


        if (text_mail.getText().toString().trim().length() < 2) {
            valide = false;
            texterror.setText("login ou mot de passe invalide");
        }

        if (text_password.getText().toString().trim().length() < 6) {
            valide = false;
            texterror.setText("login ou mot de passe invalide");
        }



        return valide;
    }

    private void connexion() {
        String mail = text_mail.getText().toString();
        String pw = text_password.getText().toString();

        if (validation() == true) {
            fAuth.signInWithEmailAndPassword(mail, pw)
                    .addOnCompleteListener(this,new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                gotomenu();
                            } else {
                                texterror.setText("login ou mot de passe invalide");
                            }
                        }

                    });
        }
    }

//betatest@gmail.com:passetest


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);

        fAuth =FirebaseAuth.getInstance();

        text_mail=findViewById(R.id.editText);
        text_password=findViewById(R.id.editText2);




        tv_login=findViewById(R.id.textView);
        tv_password=findViewById(R.id.textView3);
        texterror=findViewById(R.id.textView2);

        button_validerConnexion = findViewById(R.id.button1);
        button_validerConnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                connexion();


            }
        });
    }
}
