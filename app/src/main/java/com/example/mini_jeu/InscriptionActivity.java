package com.example.mini_jeu;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Pattern;

public class InscriptionActivity extends AppCompatActivity {
    private FirebaseAuth authentification;
    private DatabaseReference baseDonnees;


    private Button button_valider;


    private EditText text_nom,text_prenom, text_mail,text_login,text_password,text_age;
    private Spinner spinner_genre;


    private TextView tv_nom,tv_prenom,tv_mail,tv_login,tv_password,tv_age,tv_sexe,texterror;

    private void gotomenu(){
        Intent intentMenu = new Intent(InscriptionActivity.this, ConnexionActivity.class);
        startActivity(intentMenu );
    }

    public void createuser(){
        final String mail = text_mail.getText().toString();
        String pass = text_password.getText().toString();
        final String nom=text_nom.getText().toString();
        final String age=text_age.getText().toString();
        final String prenom=text_prenom.getText().toString();
        final String pseudo = text_login.getText().toString();
        this.authentification.createUserWithEmailAndPassword(mail, pass).
                addOnCompleteListener(new OnCompleteListener<AuthResult>()
                {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task)
                    {
                        if (task.isSuccessful())
                        {
                            baseDonnees.child("Utilisateurs")
                                    .child(authentification.getCurrentUser().getUid())
                                    .child("Pseudo")
                                    .setValue(pseudo);
                            baseDonnees.child("Utilisateurs")
                                    .child(authentification.getCurrentUser().getUid())
                                    .child("Mail")
                                    .setValue(mail);
                            baseDonnees.child("Utilisateurs")
                                    .child(authentification.getCurrentUser().getUid())
                                    .child("nom")
                                    .setValue(nom);
                            baseDonnees.child("Utilisateurs")
                                    .child(authentification.getCurrentUser().getUid())
                                    .child("prenom")
                                    .setValue(prenom);
                            baseDonnees.child("Utilisateurs")
                                    .child(authentification.getCurrentUser().getUid())
                                    .child("age")
                                    .setValue(age);
                            gotomenu();

                        }
                        else
                        {
                            String message = task.getException().toString();
                            Toast.makeText(InscriptionActivity.this, "Erreur : " + message, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private boolean validation(){
        texterror.setText("");
        boolean valide=true;
        if (text_nom.getText().toString().trim().length() < 2 ) {
            valide = false;
            tv_nom.setTextColor(Color.parseColor("#FF0000"));
            texterror.setText(""+texterror.getText().toString()+"Nom invalide(2 car min)\n");
        }else{
            tv_nom.setTextColor(Color.parseColor("#000000"));
        }

        if (text_prenom.getText().toString().trim().length() < 2) {
            valide = false;
            tv_prenom.setTextColor(Color.parseColor("#FF0000"));
            texterror.setText(""+texterror.getText().toString()+"Prenom invalide(2 car min)\n");
        }else{
            tv_prenom.setTextColor(Color.parseColor("#000000"));
        }





        if (text_login.getText().toString().trim().length() < 2) {
            valide = false;
            tv_login.setTextColor(Color.parseColor("#FF0000"));
            texterror.setText(""+texterror.getText().toString()+"Login invalide(2 car min)\n");
        }else{
            tv_login.setTextColor(Color.parseColor("#000000"));
        }

        if (text_password.getText().toString().trim().length() < 6) {
            valide = false;
            tv_password.setTextColor(Color.parseColor("#FF0000"));
            texterror.setText(""+texterror.getText().toString()+"¨Password invalide(6 car min)\n");
        }else{
            tv_password.setTextColor(Color.parseColor("#000000"));
        }

        if (text_age.getText().toString().trim().length() < 0) {
            valide = false;
            tv_age.setTextColor(Color.parseColor("#FF0000"));
            texterror.setText(""+texterror.getText().toString()+"Age invalide(vide)\n");
        }else{
            tv_age.setTextColor(Color.parseColor("#000000"));
        }

        if(spinner_genre.getSelectedItem() ==null ){
            valide=false;
            tv_sexe.setTextColor(Color.parseColor("#FF0000"));
            texterror.setText(""+texterror.getText().toString()+"Pas de sexe selectionné\n");
        }else{
            tv_sexe.setTextColor(Color.parseColor("#000000"));
        }


        return valide;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        text_nom=findViewById(R.id.editText);
        text_prenom=findViewById(R.id.editText2);
        text_mail=findViewById(R.id.editText3);
        text_login=findViewById(R.id.editText4);
        text_password=findViewById(R.id.editText5);
        text_age=findViewById(R.id.editText6);
        texterror=findViewById(R.id.textView14);

        tv_nom=findViewById(R.id.textView2);
        tv_prenom=findViewById(R.id.textView3);
        tv_mail=findViewById(R.id.textView6);
        tv_login=findViewById(R.id.textView7);
        tv_password=findViewById(R.id.textView8);
        tv_age=findViewById(R.id.textView5);
        tv_sexe=findViewById(R.id.textView4);


        spinner_genre=findViewById(R.id.spinner);
        String[] arraySpinner = new String[] {
                "Homme", "Femme", "Non spécifié"
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_genre.setAdapter(adapter);

        button_valider = findViewById(R.id.button1);





        button_valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(validation()==true){
                    createuser();

                }



            }
        });



        this.authentification = FirebaseAuth.getInstance();
        this.baseDonnees = FirebaseDatabase.getInstance().getReference();

    }
}
